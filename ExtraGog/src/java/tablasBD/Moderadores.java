/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablasBD;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ariel
 */
@Entity
@Table(catalog = "gogchafa", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Moderadores.findAll", query = "SELECT m FROM Moderadores m"),
    @NamedQuery(name = "Moderadores.findByIdModerador", query = "SELECT m FROM Moderadores m WHERE m.idModerador = :idModerador"),
    @NamedQuery(name = "Moderadores.findByNombre", query = "SELECT m FROM Moderadores m WHERE m.nombre = :nombre"),
    @NamedQuery(name = "Moderadores.findByApPaterno", query = "SELECT m FROM Moderadores m WHERE m.apPaterno = :apPaterno"),
    @NamedQuery(name = "Moderadores.findByApMaterno", query = "SELECT m FROM Moderadores m WHERE m.apMaterno = :apMaterno")})
public class Moderadores implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_moderador")
    private Integer idModerador;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "ap_paterno")
    private String apPaterno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "ap_materno")
    private String apMaterno;

    public Moderadores() {
    }

    public Moderadores(Integer idModerador) {
        this.idModerador = idModerador;
    }

    public Moderadores(Integer idModerador, String nombre, String apPaterno, String apMaterno) {
        this.idModerador = idModerador;
        this.nombre = nombre;
        this.apPaterno = apPaterno;
        this.apMaterno = apMaterno;
    }

    public Integer getIdModerador() {
        return idModerador;
    }

    public void setIdModerador(Integer idModerador) {
        this.idModerador = idModerador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApPaterno() {
        return apPaterno;
    }

    public void setApPaterno(String apPaterno) {
        this.apPaterno = apPaterno;
    }

    public String getApMaterno() {
        return apMaterno;
    }

    public void setApMaterno(String apMaterno) {
        this.apMaterno = apMaterno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idModerador != null ? idModerador.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Moderadores)) {
            return false;
        }
        Moderadores other = (Moderadores) object;
        if ((this.idModerador == null && other.idModerador != null) || (this.idModerador != null && !this.idModerador.equals(other.idModerador))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "tablasBD.Moderadores[ idModerador=" + idModerador + " ]";
    }
    
}
