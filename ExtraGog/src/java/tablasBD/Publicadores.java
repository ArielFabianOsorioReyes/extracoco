/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablasBD;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ariel
 */
@Entity
@Table(catalog = "gogchafa", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Publicadores.findAll", query = "SELECT p FROM Publicadores p"),
    @NamedQuery(name = "Publicadores.findByIdPublicador", query = "SELECT p FROM Publicadores p WHERE p.idPublicador = :idPublicador"),
    @NamedQuery(name = "Publicadores.findByNombre", query = "SELECT p FROM Publicadores p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "Publicadores.findByIdDesarrolladora", query = "SELECT p FROM Publicadores p WHERE p.idDesarrolladora = :idDesarrolladora")})
public class Publicadores implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_publicador")
    private Integer idPublicador;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_desarrolladora")
    private int idDesarrolladora;

    public Publicadores() {
    }

    public Publicadores(Integer idPublicador) {
        this.idPublicador = idPublicador;
    }

    public Publicadores(Integer idPublicador, String nombre, int idDesarrolladora) {
        this.idPublicador = idPublicador;
        this.nombre = nombre;
        this.idDesarrolladora = idDesarrolladora;
    }

    public Integer getIdPublicador() {
        return idPublicador;
    }

    public void setIdPublicador(Integer idPublicador) {
        this.idPublicador = idPublicador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdDesarrolladora() {
        return idDesarrolladora;
    }

    public void setIdDesarrolladora(int idDesarrolladora) {
        this.idDesarrolladora = idDesarrolladora;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPublicador != null ? idPublicador.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Publicadores)) {
            return false;
        }
        Publicadores other = (Publicadores) object;
        if ((this.idPublicador == null && other.idPublicador != null) || (this.idPublicador != null && !this.idPublicador.equals(other.idPublicador))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "tablasBD.Publicadores[ idPublicador=" + idPublicador + " ]";
    }
    
}
