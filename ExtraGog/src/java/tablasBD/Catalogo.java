/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablasBD;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ariel
 */
@Entity
@Table(catalog = "gogchafa", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Catalogo.findAll", query = "SELECT c FROM Catalogo c"),
    @NamedQuery(name = "Catalogo.findByIdJuego", query = "SELECT c FROM Catalogo c WHERE c.idJuego = :idJuego"),
    @NamedQuery(name = "Catalogo.findByTitulo", query = "SELECT c FROM Catalogo c WHERE c.titulo = :titulo"),
    @NamedQuery(name = "Catalogo.findByPrecio", query = "SELECT c FROM Catalogo c WHERE c.precio = :precio"),
    @NamedQuery(name = "Catalogo.findByIdPublicador", query = "SELECT c FROM Catalogo c WHERE c.idPublicador = :idPublicador")})
public class Catalogo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_juego")
    private Integer idJuego;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    private String titulo;
    @Basic(optional = false)
    @NotNull
    private int precio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_publicador")
    private int idPublicador;

    public Catalogo() {
    }

    public Catalogo(Integer idJuego) {
        this.idJuego = idJuego;
    }

    public Catalogo(Integer idJuego, String titulo, int precio, int idPublicador) {
        this.idJuego = idJuego;
        this.titulo = titulo;
        this.precio = precio;
        this.idPublicador = idPublicador;
    }

    public Integer getIdJuego() {
        return idJuego;
    }

    public void setIdJuego(Integer idJuego) {
        this.idJuego = idJuego;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getIdPublicador() {
        return idPublicador;
    }

    public void setIdPublicador(int idPublicador) {
        this.idPublicador = idPublicador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idJuego != null ? idJuego.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Catalogo)) {
            return false;
        }
        Catalogo other = (Catalogo) object;
        if ((this.idJuego == null && other.idJuego != null) || (this.idJuego != null && !this.idJuego.equals(other.idJuego))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "tablasBD.Catalogo[ idJuego=" + idJuego + " ]";
    }
    
}
