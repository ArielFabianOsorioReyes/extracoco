/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablasBD;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ariel
 */
@Entity
@Table(catalog = "gogchafa", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Desarrolladoras.findAll", query = "SELECT d FROM Desarrolladoras d"),
    @NamedQuery(name = "Desarrolladoras.findByIdDesarrolladora", query = "SELECT d FROM Desarrolladoras d WHERE d.idDesarrolladora = :idDesarrolladora"),
    @NamedQuery(name = "Desarrolladoras.findByNombre", query = "SELECT d FROM Desarrolladoras d WHERE d.nombre = :nombre"),
    @NamedQuery(name = "Desarrolladoras.findByUbicacion", query = "SELECT d FROM Desarrolladoras d WHERE d.ubicacion = :ubicacion")})
public class Desarrolladoras implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_desarrolladora")
    private Integer idDesarrolladora;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    private String ubicacion;

    public Desarrolladoras() {
    }

    public Desarrolladoras(Integer idDesarrolladora) {
        this.idDesarrolladora = idDesarrolladora;
    }

    public Desarrolladoras(Integer idDesarrolladora, String nombre, String ubicacion) {
        this.idDesarrolladora = idDesarrolladora;
        this.nombre = nombre;
        this.ubicacion = ubicacion;
    }

    public Integer getIdDesarrolladora() {
        return idDesarrolladora;
    }

    public void setIdDesarrolladora(Integer idDesarrolladora) {
        this.idDesarrolladora = idDesarrolladora;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDesarrolladora != null ? idDesarrolladora.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Desarrolladoras)) {
            return false;
        }
        Desarrolladoras other = (Desarrolladoras) object;
        if ((this.idDesarrolladora == null && other.idDesarrolladora != null) || (this.idDesarrolladora != null && !this.idDesarrolladora.equals(other.idDesarrolladora))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "tablasBD.Desarrolladoras[ idDesarrolladora=" + idDesarrolladora + " ]";
    }
    
}
