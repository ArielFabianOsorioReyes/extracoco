/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login.beans;

import beans.sessions.CompradorFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import tablasBD.Comprador;

/**
 *
 * @author puerc
 */
@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {
    
    private String email;
    private String nombre;

    @EJB
    private CompradorFacade compFacade; 
    
    private Comprador compradorAutenticado;

    public Comprador getCompradorAutenticado() {
        return compradorAutenticado;
    }

    public void setCompradorAutenticado(Comprador compradorAutenticado) {
        this.compradorAutenticado = compradorAutenticado;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }
    
    public String autenticar(){
        
        compradorAutenticado=compFacade.encontrarUsuarioLogin(email);
        
        if(compradorAutenticado!=null){
            if(compradorAutenticado.getNombre().equals(nombre)){
                return "Ingresar";
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Password no coincide", "Password no coincide"));
            return null;
        }
        
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"El usuario no existe", "El usuario no existe"));
        return "Ingresar";
    }
}
