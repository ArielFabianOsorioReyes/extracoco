/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import dao.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import tablasBD.Desarrolladoras;

/**
 *
 * @author puerc
 */
public class DesarrolladorasJpaController implements Serializable {

    public DesarrolladorasJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Desarrolladoras desarrolladoras) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            em.persist(desarrolladoras);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Desarrolladoras desarrolladoras) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            desarrolladoras = em.merge(desarrolladoras);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = desarrolladoras.getIdDesarrolladora();
                if (findDesarrolladoras(id) == null) {
                    throw new NonexistentEntityException("The desarrolladoras with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Desarrolladoras desarrolladoras;
            try {
                desarrolladoras = em.getReference(Desarrolladoras.class, id);
                desarrolladoras.getIdDesarrolladora();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The desarrolladoras with id " + id + " no longer exists.", enfe);
            }
            em.remove(desarrolladoras);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Desarrolladoras> findDesarrolladorasEntities() {
        return findDesarrolladorasEntities(true, -1, -1);
    }

    public List<Desarrolladoras> findDesarrolladorasEntities(int maxResults, int firstResult) {
        return findDesarrolladorasEntities(false, maxResults, firstResult);
    }

    private List<Desarrolladoras> findDesarrolladorasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Desarrolladoras.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Desarrolladoras findDesarrolladoras(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Desarrolladoras.class, id);
        } finally {
            em.close();
        }
    }

    public int getDesarrolladorasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Desarrolladoras> rt = cq.from(Desarrolladoras.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
