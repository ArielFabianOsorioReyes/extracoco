/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import tablasBD.Comprador;

/**
 *
 * @author puerc
 */
@Stateless
public class CompradorFacade extends AbstractFacade<Comprador> {

    @PersistenceContext(unitName = "ExtraGogPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CompradorFacade() {
        super(Comprador.class);
    }
    
    
    //Metodo especifico utilizando los nameQuery para buscar los usuarios
    public Comprador encontrarUsuarioLogin(String email){
        Query q=em.createNamedQuery("findByEmail", Comprador.class).setParameter("email", email);
        
        List<Comprador> Listador = q.getResultList();
        
        if(!Listador.isEmpty()){
            return Listador.get(0);
        }
        return null;
    }
}
