/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.sessions;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tablasBD.Moderadores;

/**
 *
 * @author puerc
 */
@Stateless
public class ModeradoresFacade extends AbstractFacade<Moderadores> {

    @PersistenceContext(unitName = "ExtraGogPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ModeradoresFacade() {
        super(Moderadores.class);
    }
    
}
